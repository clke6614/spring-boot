ARG BASE_IMAGE='maven:3.6.0-jdk-8'
FROM ${BASE_IMAGE} AS buildkurodomocode
# Spring Boot 為Tomcat創建默認工作目錄 作用在主機/var/lib/docker目錄下創建一個臨時文件，並鏈接到容器中#的/tmp目錄。
VOLUME /tmp

ADD application/target/application-0.0.1-SNAPSHOT.jar app.jar

COPY docker/wait-for-it.sh /wait-for-it.sh

ENV TZ=Asia/Shanghai

#容器暴露的端口
EXPOSE 7089

# java -Xms256m -Xmx512m -jar app.jar 
ENTRYPOINT ["java","-jar","app.jar"]
# CMD java -jar app.jar 