package com.starter.application;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import com.starter.dao.UserService;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.math.BigDecimal;
import com.starter.common.Animal;
/**
 * @author Bob
 * @date 2020/07/20
 */
@RestController
@RequestMapping("demo")
public class UserController {

    @GetMapping("test")
    public String test() {
        return "Hello World!";
    }

    @GetMapping("list")
    public Map list() {
        Animal product = new Animal();
        product.setProductName("kuro");
        product.setProductPrice(new BigDecimal(2.0));
        product.setProductStock(100);

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("code", 000);
        resultMap.put("message", "成功");
        resultMap.put("data", Arrays.asList(product));
        return resultMap;
    }

    @Autowired
    protected  UserService userService;

    @GetMapping("dao")
    public String daotest() {
        return userService.test();
    }    
}