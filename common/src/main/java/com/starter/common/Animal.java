package com.starter.common;

import lombok.Data;

import java.math.BigDecimal;

/** 
 *
 * @author Bob
 * @Date 2020/7/27
 */
@Data
public class Animal {

    private String productName;

    private BigDecimal productPrice;

    private int productStock;
}